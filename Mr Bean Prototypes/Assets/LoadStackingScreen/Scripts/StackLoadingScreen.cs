﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StackLoadingScreen : MonoBehaviour {

    public RectTransform m_StackingImage;

    float m_Time = 0;
	
    AsyncOperation m_NextScene;

    float m_ShakeTime = 0;

	void Start () {
        m_NextScene = SceneManager.LoadSceneAsync("Car Climber");
        m_NextScene.allowSceneActivation = false;
	}
	
	
	void Update () {
		
        Vector3 newRot = Quaternion.identity.eulerAngles;

        if (m_ShakeTime > 0.1f)
        {
            newRot.z = Random.Range(-1f,1f);

            m_StackingImage.rotation = Quaternion.Euler(newRot);

            Vector3 newPos = Vector3.zero;

            newPos.x = Random.Range(-2f, 2f);
            newPos.y = Random.Range(-2f, 2f);

            m_StackingImage.anchoredPosition = newPos;
            m_ShakeTime = 0;
        }


        m_ShakeTime += Time.deltaTime;

        if (m_Time > 2)
        {
            m_NextScene.allowSceneActivation = true;
        }



        m_Time += Time.deltaTime;
	}
}
