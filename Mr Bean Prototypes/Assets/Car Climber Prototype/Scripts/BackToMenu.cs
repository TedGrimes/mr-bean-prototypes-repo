﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToMenu : MonoBehaviour {

	public string m_MenuName;

	public void ChangeSceneToMenu()
	{
		SceneManager.LoadScene (m_MenuName);
	}

	public void ResetScene()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene().name);
	}


}
