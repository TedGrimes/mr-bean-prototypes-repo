﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(EdgeCollider2D))]

[System.Serializable]
[ExecuteInEditMode]
public class GenerateLevel : MonoBehaviour {

    public CarClimberMain m_main;

	public Transform m_CarTransform;

	public MeshFilter m_MeshFilter = null;
	public Mesh m_Mesh = null;
	public MeshRenderer m_MeshRenderer = null;
	public EdgeCollider2D m_PolygonCollider2D = null;

	[Header("Mesh Properties")]
	public Vector2 m_Size = new Vector2 (10, 10);
	public int m_Resolution = 10;

	[Header("Noise Properties")]
	public float m_NoiseScale = 0.1f;
	public float m_NoiseHeight = 1;
	public int m_Passes = 1;
	public int m_Seed = 100000;
	public bool generate = false;

	Transform m_Transform;

	Vector2 offset = new Vector2();
	Vector2 current = new Vector2();

	public Text m_ScoreText;
	int score;

    float m_StepSize = 0;

    public Transform m_endFlag;
    public bool m_placedFlag = false;

	// Use this for initialization
	void Start () {
        m_main = FindObjectOfType<CarClimberMain>();
		//GenerateMesh ();
		m_Seed = Random.Range(1000, 9000);
		m_Transform = this.transform;
		if (Application.isPlaying) {
			PositionVertsAndTriangulate ();
            //m_StepSize = Mathf.Abs(Mathf.Lerp(0, m_Size.x, Mathf.InverseLerp(0, m_Mesh.vertices.Length, 0)) - Mathf.Lerp(0, m_Size.x, Mathf.InverseLerp(0, m_Mesh.vertices.Length, 1))) * 2f;
		}
	}
	
	// Update is called once per frame
	void Update () {
        if (m_main.m_gameState == GAME_STATE.GAME_ENDED)
            return;

		offset.x = m_CarTransform.position.x;

		if (m_MeshFilter == null) {
			m_MeshFilter = this.GetComponent<MeshFilter> ();
			m_MeshRenderer = this.GetComponent<MeshRenderer> ();
			m_PolygonCollider2D = this.GetComponent<EdgeCollider2D> ();
		}

		if (m_Transform == null) {
			m_Transform = this.transform;
		}

		if (generate) {
			GenerateMesh ();
			generate = false;
		}
		else if (m_Mesh != null && Application.isPlaying) {

			if (Mathf.Abs(offset.x - current.x) >= Mathf.Abs(Mathf.Lerp (0, m_Size.x, Mathf.InverseLerp (0, m_Mesh.vertices.Length, 0)) - Mathf.Lerp (0, m_Size.x, Mathf.InverseLerp (0, m_Mesh.vertices.Length, 1))) * 2f) 
			{
                //m_StepSize = Mathf.Abs(Mathf.Lerp(0, m_Size.x, Mathf.InverseLerp(0, m_Mesh.vertices.Length, 0)) - Mathf.Lerp(0, m_Size.x, Mathf.InverseLerp(0, m_Mesh.vertices.Length, 1))) * 1f;
				PositionVertsAndTriangulate ();

				current.x = offset.x;



			}

			if (!Application.isPlaying) {
				PositionVertsAndTriangulate ();
			} else {
				score = Mathf.RoundToInt( m_CarTransform.position.x);
				m_ScoreText.text = "" + score;
			}
            PositionVertsAndTriangulate ();
		}
			

	}

	private void PositionVertsAndTriangulate()
	{
		Vector3[] verts = new Vector3[m_Mesh.vertices.Length];
		List<Vector2> verts2D = new List<Vector2>();



		for (int i = 0; i < m_Mesh.vertices.Length; i+=2) {
			//Position verts...
			Vector3 newPos = new Vector3 ();
			newPos.x = (-(m_Size.x / 2)) + Mathf.Lerp (0, m_Size.x, Mathf.InverseLerp (0, m_Mesh.vertices.Length, i)) + offset.x; 
            newPos.x = RoundToFloat(newPos.x, m_StepSize);


            newPos.y =  (m_Size.y/2);
			//Add noise to landscape...

			float noiseToAdd = 0;

			for (int p = 0; p < m_Passes; p++) {
                noiseToAdd += Mathf.PerlinNoise (((newPos.x + 1000) * (m_NoiseScale * (p + 1)))  , m_Seed) / (p + 1);

			}



			noiseToAdd = Mathf.InverseLerp (0, m_Passes - 1, noiseToAdd);
			//Debug.Log (noiseToAdd);
			noiseToAdd = (noiseToAdd * 2) - 1;

			//noiseToAdd *= m_NoiseHeight;

			noiseToAdd *= Mathf.Abs ((newPos.x * 0.05f) - 0);
			//noiseToAdd = Mathf.Clamp (noiseToAdd, -Mathf.Abs(m_NoiseHeight), Mathf.Abs(m_NoiseHeight));

			newPos.y += noiseToAdd;


			newPos.z = m_Transform.localPosition.z; 
			verts [i] = newPos;
			verts2D.Add(newPos);

            if (newPos.x > 200 && !m_placedFlag)
            {
                
                m_endFlag.position = transform.position + newPos;
                m_placedFlag = true;
            }


			newPos = new Vector3 ();
			newPos.x = ( -(m_Size.x / 2)) + Mathf.Lerp (0, m_Size.x, Mathf.InverseLerp (0, m_Mesh.vertices.Length, i)) + offset.x; 
            newPos.x = RoundToFloat(newPos.x, m_StepSize);
            newPos.y = -(m_Size.y/2);
			newPos.z = m_Transform.localPosition.z;
			verts [i + 1] = newPos;




		}




		m_MeshFilter.sharedMesh.vertices = verts;
		m_MeshFilter.sharedMesh.RecalculateNormals ();
		m_MeshFilter.sharedMesh.RecalculateBounds ();
		//m_MeshFilter.sharedMesh.vertices = m_Mesh.vertices;
		m_PolygonCollider2D.points =  verts2D.ToArray();

	}

    float RoundToFloat(float toRound, float stepSize)
    {
        if(toRound >= Mathf.Round(toRound))
        {
            if(toRound <= Mathf.Round(toRound) + (stepSize / 2))
            {
                return Mathf.Round(toRound);
            }
            else
            {
                return Mathf.Round(toRound) + stepSize;
            }
        }
        else
        {
            if (toRound >= Mathf.Round(toRound) - (stepSize / 2))
            {
                return Mathf.Round(toRound);
            }
            else
            {
                return Mathf.Round(toRound) - stepSize;
            }
        }
    }

	private void GenerateMesh()
	{
		m_Mesh = new Mesh ();

		Vector3[] verts = new Vector3[m_Resolution * 2];
		List<int> triangs = new List<int> ();

	
		for (int i = 0; i < verts.Length - 2; i+= 2) {
			triangs.Add(i);
			triangs.Add(i + 2);
			triangs.Add(i + 1);

			triangs.Add(i + 1);
			triangs.Add(i + 2);
			triangs.Add(i + 3);
		}


		m_MeshFilter.sharedMesh = m_Mesh;
		m_MeshFilter.sharedMesh.vertices = verts;
		m_MeshFilter.sharedMesh.triangles = triangs.ToArray();
		m_MeshFilter.sharedMesh.MarkDynamic ();

		PositionVertsAndTriangulate ();
	}
}
