﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCar : MonoBehaviour {



    public Transform m_StartingPosition;

    public GameObject[] m_CarPrefabs;



	void Start () {
        GameObject newCar = (GameObject) Instantiate(m_CarPrefabs[PlayerPrefs.GetInt("Car")]);
        newCar.transform.position = m_StartingPosition.position;
        Camera.main.GetComponent<CameraFollow2D>().followObject = newCar.GetComponentInChildren<CarControl>().transform.gameObject;
        FindObjectOfType<GenerateTerrain>().m_CarTransform = newCar.GetComponentInChildren<CarControl>().transform;
	}
	
	
	
}
