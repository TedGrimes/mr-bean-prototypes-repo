﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Consumable {Glider, SpeedBoost, FreezeStack, Spring, ExtendoPlank, Straps}
public enum GAME_STATE {GAME_RUNNING, GAME_ENDED};
public class CarClimberMain : MonoBehaviour {

    GenerateLevel m_genLevel;
    StackObjectsPhysical m_objectStacker;

    public GAME_STATE m_gameState = GAME_STATE.GAME_RUNNING;

    [System.Serializable]
    public class ConsumableItem
    {
        public Consumable m_Consumable;
        public float m_Time;
        public Text m_Text;
        public Image m_Image;
        public bool m_Use = false;
        public bool m_InUse = false;
    }
    public Sprite[] m_ConsumableSprites;

    public ConsumableItem[] m_Consumables;

    public GameObject m_Plank;

    public RectTransform m_completePanel;
    public RectTransform m_failedPanel;

    public Transform m_playerCar;
    public Transform m_finishLine;

	void Start () {
        m_genLevel = FindObjectOfType<GenerateLevel>();
        m_objectStacker = FindObjectOfType<StackObjectsPhysical>();
        LoadConsumables();
	}
    void LoadConsumables()
    {
        int[] consumableID = new int[PlayerPrefs.GetInt("ConsumeLength")];
        for (int i = 0; i < consumableID.Length; i++)
        {
            consumableID[i] = PlayerPrefs.GetInt("Consume" + i);
        }


        for (int i = 0; i < m_Consumables.Length; i++)
        {
            switch (consumableID[i])
            {
                case -1:
                    m_Consumables[i].m_Text.text = "None";
                    break;
                case 0:
                    m_Consumables[i].m_Consumable = Consumable.Glider;
                    m_Consumables[i].m_Text.text = "Glider";
                    m_Consumables[i].m_Image.sprite = m_ConsumableSprites[0];
                    break;
                case 1:
                    m_Consumables[i].m_Consumable = Consumable.SpeedBoost;
                    m_Consumables[i].m_Text.text  = "Speed";
                    m_Consumables[i].m_Image.sprite = m_ConsumableSprites[1];
                    break;
                case 2:
                    m_Consumables[i].m_Consumable = Consumable.FreezeStack;
                    m_Consumables[i].m_Text.text  = "Freeze";
                    m_Consumables[i].m_Image.sprite = m_ConsumableSprites[2];
                    break;
                case 3:
                    m_Consumables[i].m_Consumable = Consumable.Spring; 
                    m_Consumables[i].m_Text.text  = "Spring";
                    m_Consumables[i].m_Image.sprite = m_ConsumableSprites[3];
                    break;
                case 4:
                    m_Consumables[i].m_Consumable = Consumable.ExtendoPlank;
                    m_Consumables[i].m_Text.text  = "Plank";
                    m_Consumables[i].m_Image.sprite = m_ConsumableSprites[4];
                    break;
                case 5:
                    m_Consumables[i].m_Consumable = Consumable.Straps;
                    m_Consumables[i].m_Text.text  = "Straps";
                    m_Consumables[i].m_Image.sprite = m_ConsumableSprites[5];
                    break;
            }
        }

    }
	
	void Update () {
        if (m_objectStacker.m_itemCount <= 0 && m_gameState != GAME_STATE.GAME_ENDED)
        {
            //StartCoroutine(DeliveryFailedScreen());
        }
	}

    public IEnumerator DeliveryFailedScreen()
    {
        m_gameState = GAME_STATE.GAME_ENDED;
        m_failedPanel.anchoredPosition = Vector2.zero;
        yield return null;
    }

    public IEnumerator DeliveryCompleteScreen()
    {
        m_gameState = GAME_STATE.GAME_ENDED;
        m_completePanel.anchoredPosition = Vector2.zero;
        yield return null;
    }

    public void ConsumableButton(int i)
    {
        m_Consumables[i].m_Use = true;
    }
}
