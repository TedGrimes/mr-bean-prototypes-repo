﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow2D : MonoBehaviour {

    CarClimberMain m_main;

    public GameObject followObject;
    public Vector3 startPos;

	public Vector2 offset = new Vector2();
	[System.Serializable]
	public class LockFollow
	{
		public bool x = true;
		public bool y = false;
	}
	public LockFollow lockFollow;
	// Use this for initialization
	void Start () {
        startPos = this.transform.position;
        m_main = FindObjectOfType<CarClimberMain>();
    }
	
	// Update is called once per frame
	public void LateUpdate () {

        if (m_main.m_gameState == GAME_STATE.GAME_ENDED)
            return;

        Vector3 newPos = this.transform.position;

		if (lockFollow.x) {
			newPos.x = followObject.transform.position.x;
		}
		if (lockFollow.y) {
			newPos.y = followObject.transform.position.y;
		}
			
				

        

		this.transform.position = newPos + (Vector3) offset;
	}
}
