﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CarControl : MonoBehaviour {

    CarClimberMain m_Main;
    StackObjectsPhysical m_ObjectStacker;
    GenerateTerrain m_genLevel;

    public Transform m_PlankPos;

	public WheelJoint2D m_FrontWheelJoint;
	public WheelJoint2D m_BackWheelJoint;

	Rigidbody2D m_ThisRigid;

	public float m_Speed = 100;
	public float m_Torque = 100;

	bool forward = false;
	bool backwards = false;


    JointMotor2D tempMotorFront;
    JointMotor2D tempMotorBack;



	// Use this for initialization
	void Start () {
        m_Main = FindObjectOfType<CarClimberMain>();
        m_genLevel = FindObjectOfType<GenerateTerrain>();
		m_ThisRigid = this.GetComponent<Rigidbody2D> ();
        m_ObjectStacker = this.GetComponent<StackObjectsPhysical>();
        tempMotorFront = m_FrontWheelJoint.motor;
        tempMotorBack = m_BackWheelJoint.motor;
	}
	


	// Update is called once per frame
	void FixedUpdate () {
		
        if (m_Main.m_gameState == GAME_STATE.GAME_ENDED)
            return;

		if (Input.GetKey ("w")|| forward) {
            
			tempMotorFront.motorSpeed =Mathf.Lerp(tempMotorFront.motorSpeed, -m_Speed,  Time.deltaTime* 1f);
			tempMotorBack.motorSpeed = Mathf.Lerp(tempMotorBack.motorSpeed, -m_Speed,  Time.deltaTime* 1f);
            m_ThisRigid.AddTorque (m_Torque * Time.deltaTime );
            m_FrontWheelJoint.motor = tempMotorFront;
            m_BackWheelJoint.motor = tempMotorBack;

		} else if (Input.GetKey ("s")|| backwards) {
            
			tempMotorFront.motorSpeed = Mathf.Lerp(tempMotorFront.motorSpeed, m_Speed,  Time.deltaTime* 1f);
			tempMotorBack.motorSpeed = Mathf.Lerp(tempMotorBack.motorSpeed, m_Speed,  Time.deltaTime* 1f);
            m_ThisRigid.AddTorque (-m_Torque * Time.deltaTime );
            m_FrontWheelJoint.motor = tempMotorFront;
            m_BackWheelJoint.motor = tempMotorBack;
		} else {
            
            //tempMotorFront.motorSpeed = Mathf.Lerp(tempMotorFront.motorSpeed, 0,  Time.deltaTime* 0.1f);
            //tempMotorBack.motorSpeed = Mathf.Lerp(tempMotorBack.motorSpeed, 0,  Time.deltaTime* 0.1f);
            tempMotorFront.motorSpeed = m_FrontWheelJoint.connectedBody.GetComponent<Rigidbody2D>().angularVelocity;
            tempMotorBack.motorSpeed = m_BackWheelJoint.connectedBody.GetComponent<Rigidbody2D>().angularVelocity;
            //Debug.Log(tempMotorFront.motorSpeed);
            m_FrontWheelJoint.useMotor = false;
            m_BackWheelJoint.useMotor = false;
		}

			

		if (Input.GetMouseButton (0)) {

			if (Input.mousePosition.x > Screen.width / 2) {
				forward = true;
				backwards = false;
			} else {
				forward = false;
				backwards = true;
			}
		} else {
			forward = false;
			backwards = false;
		}
        for (int i = 0; i < m_Main.m_Consumables.Length; i++)
        {
            if (m_Main.m_Consumables[i].m_Use && !m_Main.m_Consumables[i].m_InUse)
            {
                switch (m_Main.m_Consumables[i].m_Consumable)
                {
                    case Consumable.Glider:
                        StartCoroutine(Glider(m_Main.m_Consumables[i].m_Time, i));
                        break;
                    case Consumable.SpeedBoost:
                        StartCoroutine(SpeedBoost(m_Main.m_Consumables[i].m_Time, i));
                        break;
                    case Consumable.FreezeStack:
                        StartCoroutine(FreezeStack(m_Main.m_Consumables[i].m_Time, i));
                        break;
                    case Consumable.Spring:
                        StartCoroutine(SpringBoost(m_Main.m_Consumables[i].m_Time, i));
                        break;
                    case Consumable.ExtendoPlank:
                        StartCoroutine(Plank(m_Main.m_Consumables[i].m_Time, i));
                        break;
                    case Consumable.Straps:
                        StartCoroutine(Straps(m_Main.m_Consumables[i].m_Time, i));
                        break;
                }
                m_Main.m_Consumables[i].m_Use = false;
            }
        }

        if (m_genLevel.m_placedFlag && transform.position.x > m_Main.m_finishLine.position.x)
        {
            StartCoroutine(m_Main.DeliveryCompleteScreen());
        }
		
	}

    public IEnumerator Glider(float m_Time, int i)
    {
        m_Main.m_Consumables[i].m_InUse = true;
        float t = 0;
        while (t < m_Time)
        {
            this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * Time.deltaTime *1500);

            t += Time.deltaTime;
            yield return null;
        }


        m_Main.m_Consumables[i].m_InUse = false;
        yield return null;
    }

    public IEnumerator SpeedBoost(float m_Time, int i)
    {
        m_Main.m_Consumables[i].m_InUse = true;
        float startSpeed = m_Speed;
        m_Speed = m_Speed * 1.5f;
        float t = 0;
        while (t < m_Time)
        {

            forward = true;
            //this.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Time.deltaTime * 1000);

            t += Time.deltaTime;
            yield return null;
        }
        m_Speed = startSpeed;
        m_Main.m_Consumables[i].m_InUse = false;

        yield return null;
    }

    public IEnumerator FreezeStack(float m_Time, int i)
    {
        m_Main.m_Consumables[i].m_InUse = true;

        List<Transform> tempTransforms = new List<Transform>();

        for (int o = 0; o < m_ObjectStacker.m_StackedItemsTransform.Count; o++) {
            if (m_ObjectStacker.m_StackedItemsTransform[o].GetComponent<SpringJoint2D>().enabled)
            {
                m_ObjectStacker.m_StackedItemsTransform[o].GetComponent<Collider2D>().enabled = false;
                m_ObjectStacker.m_StackedItemsTransform[o].GetComponent<SpringJoint2D>().enabled = false;
                m_ObjectStacker.m_StackedItemsTransform[o].GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                m_ObjectStacker.m_StackedItemsTransform[o].GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                m_ObjectStacker.m_StackedItemsTransform[o].parent = this.gameObject.transform;
                tempTransforms.Add(m_ObjectStacker.m_StackedItemsTransform[o]);
            }

        }

        float t = 0;
        while (t < m_Time)
        {



            t += Time.deltaTime;
            yield return null;
        }

        for (int o = 0; o < tempTransforms.Count; o++) {

            tempTransforms[o].GetComponent<Collider2D>().enabled = true;
            tempTransforms[o].GetComponent<SpringJoint2D>().enabled = true;
            tempTransforms[o].GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            m_ObjectStacker.m_StackedItemsTransform[o].GetComponent<Rigidbody2D>().velocity = m_ThisRigid.velocity;
            tempTransforms[o].parent = null;


        }

        m_Main.m_Consumables[i].m_InUse = false;

        yield return null;
    }

    public IEnumerator SpringBoost(float m_Time, int i)
    {
        m_Main.m_Consumables[i].m_InUse = true;
        this.GetComponent<Rigidbody2D>().AddForce(transform.up  * 2000);



        m_Main.m_Consumables[i].m_InUse = false;

        yield return null;
    }

    public IEnumerator Plank(float m_Time, int i)
    {
        m_Main.m_Consumables[i].m_InUse = true;

        float originalHeight = m_Main.m_Plank.transform.localScale.y;
        Transform newPlank = Instantiate(m_Main.m_Plank).transform;
        newPlank.position = m_PlankPos.position;
        newPlank.rotation = m_PlankPos.rotation;
        newPlank.localScale = new Vector3(newPlank.localScale.x, 0, newPlank.localScale.z);
        newPlank.transform.parent = transform;
        newPlank.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
        float t = 0;
        while (t < 1)
        {

            newPlank.localScale = new Vector3(newPlank.localScale.x, Mathf.Lerp(0,originalHeight, t), newPlank.localScale.z);
            newPlank.position = Vector3.Lerp(m_PlankPos.position,m_PlankPos.position + (m_PlankPos.up * (originalHeight/6)), t);
            t += Time.deltaTime;
            yield return null;
        }
        newPlank.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        newPlank.GetComponent<Rigidbody2D>().AddTorque(-150);
        newPlank.transform.parent = null;
        newPlank.localScale = new Vector3(newPlank.localScale.x, originalHeight, newPlank.localScale.z);
        m_Main.m_Consumables[i].m_InUse = false;

        yield return null;
    }

    public IEnumerator Straps(float m_Time, int i)
    {
        m_Main.m_Consumables[i].m_InUse = true;
        List<float> startFreq = new List<float>();
        for (int o = 0; o < m_ObjectStacker.m_StackedItemsTransform.Count; o++)
        {
            if (m_ObjectStacker.m_StackedItemsTransform[o].GetComponent<SpringJoint2D>().enabled)
            {
                m_ObjectStacker.m_StackedItemsTransform[o].GetComponent<SpringJoint2D>().frequency = 30;
                startFreq.Add(m_ObjectStacker.m_StackedItemsTransform[o].GetComponent<SpringJoint2D>().frequency);
            }

        }

        float t = 0;
        while (t < m_Time)
        {

           

            t += Time.deltaTime;
            yield return null;
        }

        for (int o = 0; o < m_ObjectStacker.m_StackedItemsTransform.Count; o++)
        {
            if (m_ObjectStacker.m_StackedItemsTransform[o].GetComponent<SpringJoint2D>().enabled)
            {
                m_ObjectStacker.m_StackedItemsTransform[o].GetComponent<SpringJoint2D>().frequency = startFreq[o];
            }

        }
        m_Main.m_Consumables[i].m_InUse = false;

        yield return null;
    }


}
