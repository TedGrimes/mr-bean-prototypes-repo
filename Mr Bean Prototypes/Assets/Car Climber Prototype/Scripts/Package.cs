﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Package : MonoBehaviour {

    StackObjectsPhysical m_ObjectStacker;
	SpringJoint2D spring;

    public float m_PackageHeight = 0.52f;
    public Transform m_PackageCentre = null;

    public Transform m_startPoint = null;
    public Transform m_endPoint = null;



	void Start()
	{
        m_ObjectStacker = FindObjectOfType<StackObjectsPhysical>();
		spring = this.GetComponent<SpringJoint2D> ();
	}

	void Update()
	{
        if (spring != null && spring.connectedBody != null && this.GetComponent<SpringJoint2D>().enabled) {
            if (Mathf.Abs( Vector3.Angle (spring.connectedBody.transform.up,  this.transform.up)) > 35) {
				//spring.enabled = false;
                //m_ObjectStacker.m_StackedItemsTransform.Remove(this.transform);

                for (int o = m_ObjectStacker.m_StackedItemsTransform.IndexOf(transform); o < m_ObjectStacker.m_StackedItemsTransform.Count; o++)
                {
                    //m_ObjectStacker.m_StackedItemsTransform[o].GetComponent<SpringJoint2D>().enabled = false;
                    //m_ObjectStacker.m_StackedItemsTransform[o].GetComponent<Rigidbody2D>().AddForce((Vector2)(this.transform.position - m_ObjectStacker.transform.position).normalized  * 30);
                    //m_ObjectStacker.m_itemCount--;
                }

			}
		}

	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.transform.tag == "Floor") 
        {
            m_ObjectStacker.DisconnectBungies();
			spring.enabled = false;
            //m_ObjectStacker.m_lineRenderer1.enabled = false;
            //m_ObjectStacker.m_lineRenderer2.enabled = false;
		}
	}



}
	

