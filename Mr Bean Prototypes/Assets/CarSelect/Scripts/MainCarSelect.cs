﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainCarSelect : MonoBehaviour {

    public GameObject[] m_CarPrefabs;

    Transform[] m_SpawnedCars;
    int m_SelectedCar = 0;

    public Vector3 m_Offset;

    public bool m_ShowInventions;
    public RectTransform m_InventionPanel;
    public int m_InventionSelection = 0;

    public Image[] m_Button;
    public Text[] m_ButtonName;
    public Sprite[] m_ConsumableSprites;
    public string[] m_ConsumableNames;

	// Use this for initialization
	void Start () {
		
        m_SelectedCar = PlayerPrefs.GetInt("Car");

        m_SpawnedCars = new Transform[m_CarPrefabs.Length];
        for (int i = 0; i < m_SpawnedCars.Length; i++)
        {
            m_SpawnedCars[i] = Instantiate(m_CarPrefabs[i]).transform;
            Vector3 newPos = Vector3.zero;

            newPos.x = i * 8;

            m_SpawnedCars[i].position = newPos;

        }
        m_InventionPanel.gameObject.SetActive(false);
        for (int i = 0; i < m_Button.Length; i++)
        {
            m_Button[i].sprite = m_ConsumableSprites[PlayerPrefs.GetInt("Consume" + i)]; 
            m_ButtonName[i].text  = m_ConsumableNames[PlayerPrefs.GetInt("Consume" + i)];
        }
        PlayerPrefs.SetInt("ConsumeLength", m_Button.Length);

	}
	
	// Update is called once per frame
	void Update () {
		
        Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, new Vector3(m_SpawnedCars[m_SelectedCar].position.x,Camera.main.transform.position.y, Camera.main.transform.position.z), 0.2f);

        if (m_ShowInventions)
        {
            m_InventionPanel.gameObject.SetActive(true);

            for (int i = 0; i < m_Button.Length; i++)
            {
                m_Button[i].sprite = m_ConsumableSprites[PlayerPrefs.GetInt("Consume" + i)]; 
                m_ButtonName[i].text  = m_ConsumableNames[PlayerPrefs.GetInt("Consume" + i)];
            }

            if (!EventSystem.current.IsPointerOverGameObject() && Input.GetMouseButtonDown(0))
            {
                //m_InventionPanel.gameObject.SetActive(false);
                //m_ShowInventions = false;
            }
        }
        else
        {
            m_InventionPanel.gameObject.SetActive(false);
        }

	}

    public void Select(int i)
    {
        if (m_SelectedCar + i >= 0 && m_SelectedCar + i < m_SpawnedCars.Length)
        {
            m_SelectedCar += i;
        }
    }

    public void GoToNextLevel()
    {
        PlayerPrefs.SetInt("Car", m_SelectedCar);
        PlayerPrefs.Save();

        SceneManager.LoadScene("LoadStackingScreenScene");

    }

    public void BringUpInventionSelection(int i)
    {
        if (m_ShowInventions)
        {
            if (i == m_InventionSelection)
            {
                m_ShowInventions = false;
            }
            else
            {
                m_InventionSelection = i;
            }
        }
        else
        {
            m_ShowInventions = true;
            m_InventionSelection = i;
        }

    }

    public void SelectConsumable(int consumable)
    {
        
        PlayerPrefs.SetInt("Consume" + m_InventionSelection, consumable);

        if (m_InventionSelection == 0)
        {
            PlayerPrefs.SetInt("ConsumeOne", consumable);
        }
        else
        {
            PlayerPrefs.SetInt("ConsumeTwo", consumable);
        }
        PlayerPrefs.Save();
    }
}
