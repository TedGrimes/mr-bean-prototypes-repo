﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StackObjects : MonoBehaviour {

	public GameObject[] m_Packages;
	public List<Transform> m_SpawnedPackages = new List<Transform>();

	public Transform m_CarBody;
	Vector3 m_carBodyPos;
	public Transform m_StackFromObject;

	public AnimationCurve m_CarBounceCurve;
	public AnimationCurve m_CarBounceCurveUp;
	public AnimationCurve m_ObjectDropCurve;

	public GameObject[] m_DisplayWindow;
	public GameObject[] m_DisplayWindowItems;
	public List<int> m_ItemRequestID = new List<int>();

	public List<Image> m_UsedItems = new List<Image> ();
	public List<int> m_UsedItemsID = new List<int> ();

	List<int> m_DestroyObjectWait = new List<int>();

	bool m_AnimRunning = false;

	void Start () {
		m_carBodyPos = m_CarBody.position;
		GenerateRequest ();

	}

	void GenerateRequest()
	{
		int requestAmount = Random.Range (3, 6);

		for (int i = 0; i < requestAmount; i++) {
			int requestType = Random.Range (0, m_DisplayWindowItems.Length);
			GameObject newItem = (GameObject)Instantiate (m_DisplayWindowItems [requestType]);
			newItem.transform.parent = m_DisplayWindow [i].transform;
			newItem.transform.localPosition = Vector3.zero;
			newItem.transform.localScale = m_DisplayWindowItems [requestType].transform.localScale;
		}
	}

	void Update () {
		if (!m_AnimRunning && m_DestroyObjectWait.Count != 0) 
		{
			StartCoroutine (DeleteStackObject (m_DestroyObjectWait [0]));
			m_DestroyObjectWait.RemoveAt (0);
		}
	}

	IEnumerator StackObject(int objectRef)
	{
		//m_AnimRunning = true;
		Transform newPackage = Instantiate (m_Packages [objectRef]).transform;

		newPackage.position = m_StackFromObject.position;
		newPackage.position += new Vector3(0, 0.52f, 0);
		m_StackFromObject.position = newPackage.position + new Vector3(0, 0.52f , 0);
		newPackage.parent = m_CarBody;
		m_SpawnedPackages.Add (newPackage);

		float t = 0;
		while (t < 1) 
		{
			Vector3 tempPos = m_CarBody.position;

			tempPos.y = m_carBodyPos.y + (m_CarBounceCurve.Evaluate (t) * 1);

			m_CarBody.position = tempPos;

			t += Time.deltaTime * 1;
			yield return null;
		}
		m_AnimRunning = false;

		yield return null;
	}

	IEnumerator DeleteStackObject(int objectRef)
	{
		m_AnimRunning = true;
		List<Transform> itemAbove = new List<Transform> ();
		List<Vector3> itemAboveStartPos = new List<Vector3> ();


		for (int i = objectRef + 1; i < m_SpawnedPackages.Count; i++) {
			itemAbove.Add (m_SpawnedPackages [i]);
			itemAboveStartPos.Add (m_SpawnedPackages [i].position);
		}

		Destroy (m_SpawnedPackages [objectRef].gameObject);
		m_SpawnedPackages.RemoveAt (objectRef);
		m_UsedItems.RemoveAt (objectRef);
		m_UsedItemsID.RemoveAt (objectRef);

		m_StackFromObject.position -= new Vector3(0, 1.04f , 0);
		float t = 0;
		while (t < 1) 
		{
			Vector3 tempPos = m_CarBody.position;

			tempPos.y = m_carBodyPos.y + (m_CarBounceCurveUp.Evaluate (t) * 1);

			m_CarBody.position = tempPos;

			for (int i = 0; i < itemAbove.Count; i++) {
				itemAbove[i].position = new Vector3(itemAbove[i].position.x, Mathf.LerpUnclamped(itemAboveStartPos[i].y, itemAboveStartPos[i].y - 1.04f,m_ObjectDropCurve.Evaluate(t))  , itemAbove[i].position.z);
			}

			t += Time.deltaTime * 3;
			yield return null;
		}
		for (int i = 0; i < itemAbove.Count; i++) {
			itemAbove[i].position = new Vector3(itemAbove[i].position.x,  itemAboveStartPos[i].y - 1.04f , itemAbove[i].position.z);
		}


		m_AnimRunning = false;
		yield return null;

	}

	float GetSpriteHeight(Transform sprite)
	{


		return sprite.localScale.y / sprite.GetComponent<SpriteRenderer>().sprite.bounds.size.y;

	}

	public void StackNavy(Image button)
	{
		
		if (!m_UsedItems.Contains (button)) {
			button.color = Color.grey;
			m_UsedItems.Add (button);
			m_UsedItemsID.Add (0);
			StopAllCoroutines ();
			StartCoroutine (StackObject (0));
		} else {
			//StopAllCoroutines ();
			m_DestroyObjectWait.Add (m_UsedItems.IndexOf (button));
			button.color = Color.white;

		}

	}

	public void StackPink(Image button)
	{
		
		if (!m_UsedItems.Contains (button)) {
			button.color = Color.grey;
			m_UsedItems.Add (button);
			m_UsedItemsID.Add (1);
			StopAllCoroutines ();
			StartCoroutine (StackObject (1));
		} else {
			//StopAllCoroutines ();
			m_DestroyObjectWait.Add (m_UsedItems.IndexOf (button));
			button.color = Color.white;

		}

	}

	public void StackTeal(Image button)
	{
		
		if (!m_UsedItems.Contains (button)) {
			button.color = Color.grey;
			m_UsedItems.Add (button);
			m_UsedItemsID.Add (2);
			StopAllCoroutines ();
			StartCoroutine (StackObject (2));
		} else {
			//StopAllCoroutines ();
			m_DestroyObjectWait.Add (m_UsedItems.IndexOf (button));
			button.color = Color.white;

		}

	}
}
