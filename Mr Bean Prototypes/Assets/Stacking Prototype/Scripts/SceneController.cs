﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {

	public string sceneToLoad = "";
	StackObjects main; 

	void Start()
	{
		main = FindObjectOfType<StackObjects> ();



	}

	public void SwitchScene()
	{
		


		SaveScene ();


		SceneManager.LoadScene (sceneToLoad);
	}


	void SaveScene()
	{
		PlayerPrefs.SetInt ("ObjectListLength", main.m_UsedItemsID.Count);

		for (int i = 0; i < main.m_UsedItemsID.Count; i++) {
			PlayerPrefs.SetInt ("Object" + i, main.m_UsedItemsID [i]);
		}

	}

	void LoadScene()
	{

	}
}
