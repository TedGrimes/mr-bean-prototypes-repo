﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GenerateTerrain))]
public class TerrianGeneratorEditor : Editor {

    GenerateTerrain m_genTerrain;
	
    private AnimationCurve m_noiseDisplay;
    private Keyframe[] m_keyFrames;

    private float m_areaSample = 0.6f;
    private float m_scrubbing = 0;
    private float m_displayHeight = 3;

    private bool[] m_openedWaves;

    public void OnEnable()
    {
        m_genTerrain = (GenerateTerrain)target;
        m_keyFrames = new Keyframe[200];
        m_openedWaves = new bool[m_genTerrain.m_curveDataArray.Count];
    }

    public override void OnInspectorGUI()
    {

        m_genTerrain.m_Resolution = EditorGUILayout.IntField("Resolution", m_genTerrain.m_Resolution);
        m_genTerrain.m_StepSize = EditorGUILayout.FloatField("Step Size", m_genTerrain.m_StepSize);
        m_genTerrain.m_yOffset = EditorGUILayout.FloatField("Y Offset", m_genTerrain.m_yOffset);
        m_areaSample = EditorGUILayout.FloatField("Area Sample", m_areaSample);
        m_scrubbing = EditorGUILayout.FloatField("Scrubbing", m_scrubbing);

        SetKeysFinal(m_genTerrain.m_curveDataArray);
        GUI.backgroundColor = Color.cyan;
        GUI.color = Color.green;
        EditorGUILayout.CurveField(m_noiseDisplay,GUILayout.Height(100));
        GUI.color = Color.white;
        GUI.backgroundColor = Color.white;

        if (GUILayout.Button("Generate"))
        {
            m_genTerrain.GenerateMesh();
        }

        int toRemove = -1;
        for (int i = 0; i < m_genTerrain.m_curveDataArray.Count; i++)
        {
            if (m_genTerrain.m_curveDataArray[i] != null)
            {

                CurveData curveData = m_genTerrain.m_curveDataArray[i];
                GUI.color = Color.grey;
                GUILayout.BeginVertical(EditorStyles.helpBox);
                GUI.color = Color.white;
                GUILayout.BeginHorizontal();
                curveData.m_isActive = EditorGUILayout.Toggle(curveData.m_isActive);
                GUILayout.Label(curveData.name, EditorStyles.boldLabel);
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("-"))
                {
                    toRemove = i;
                }
                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();
                GUILayout.Space(15);
                m_openedWaves[i] = EditorGUILayout.Foldout(m_openedWaves[i], "Properties");
                GUILayout.EndHorizontal();
                if (m_openedWaves[i])
                {
                    

                    curveData.m_waveType = (WaveType)EditorGUILayout.EnumPopup("Wave Type", curveData.m_waveType);
                    curveData.m_blendMode = (BlendMode)EditorGUILayout.EnumPopup("Blend Mode", curveData.m_blendMode);
                    curveData.m_frequency = EditorGUILayout.FloatField("Frequency", curveData.m_frequency);
                    curveData.m_amplitude = EditorGUILayout.FloatField("Amplitude", curveData.m_amplitude);
                    GUILayout.Space(5);

                    EditorGUILayout.LabelField("Limits", EditorStyles.boldLabel);
                    curveData.m_low = EditorGUILayout.Slider("Low", curveData.m_low, 0f, 1f);
                    curveData.m_high = EditorGUILayout.Slider("Heigh", curveData.m_high, 0f, 1f);



                    // Show Curve Affect
                    GUILayout.Label("Curve Affect", EditorStyles.boldLabel);
                    int toRemoveAffect = -1;
                    for (int affect = 0; affect < curveData.m_affect.Count; affect++) 
                    {
                        CurveAffectData curveAffect = curveData.m_affect[affect];
                        GUILayout.BeginVertical(EditorStyles.helpBox);

                        if (curveData.m_affect[affect] != null)
                        {
                            GUILayout.BeginHorizontal();
                            curveAffect.m_isActive = EditorGUILayout.Toggle(curveAffect.m_isActive);
                            GUILayout.Label(curveAffect.name);
                            GUILayout.FlexibleSpace();
                            if (GUILayout.Button("-"))
                            {
                                toRemoveAffect = affect;
                            }
                            GUILayout.EndHorizontal();
                            curveAffect.m_waveType = (WaveType)EditorGUILayout.EnumPopup("Wave Type", curveAffect.m_waveType);
                            curveAffect.m_blendMode = (BlendMode)EditorGUILayout.EnumPopup("Blend Mode", curveAffect.m_blendMode);
                            curveAffect.m_frequency = EditorGUILayout.FloatField("Frequency", curveAffect.m_frequency);
                            curveAffect.m_amplitude = EditorGUILayout.FloatField("Amplitude", curveAffect.m_amplitude);
                            GUILayout.Space(5);
                            EditorGUILayout.LabelField("Limits", EditorStyles.boldLabel);
                            curveAffect.m_low = EditorGUILayout.Slider("Low", curveAffect.m_low, 0f, 1f);
                            curveAffect.m_high = EditorGUILayout.Slider("Heigh", curveAffect.m_high, 0f, 1f);
                            SetKeysAffect(curveAffect);
                            GUI.backgroundColor = new Color(0.8f, 0.8f, 0.6f);
                            EditorGUILayout.CurveField(m_noiseDisplay,GUILayout.Height(60));
                            GUI.backgroundColor = Color.white;
                        }
                        else
                        {
                            GUILayout.BeginHorizontal();
                            GUILayout.Label("Null Affect");
                            GUILayout.FlexibleSpace();
                            if (GUILayout.Button("-"))
                            {
                                toRemoveAffect = affect;
                            }
                            GUILayout.EndHorizontal();
                            curveData.m_affect[affect] = (CurveAffectData)EditorGUILayout.ObjectField(curveAffect, typeof(CurveAffectData), true);
                        }
                        GUILayout.EndVertical();
                        GUILayout.Space(5);
                    }
                    if (toRemoveAffect != -1)
                    {
                        curveData.m_affect.RemoveAt(toRemoveAffect);
                    }

                    if (GUILayout.Button("Add Curve Affect"))
                    {
                        curveData.m_affect.Add(null);
                    }



                    GUILayout.Space(5);
                    EditorGUILayout.LabelField("Noise Example", EditorStyles.boldLabel);
                    SetKeys(curveData);
                    GUI.backgroundColor = new Color(0, 1, 0.6f);
                    EditorGUILayout.CurveField(m_noiseDisplay,GUILayout.Height(100));
                    GUI.backgroundColor = Color.white;

                }
                GUILayout.EndVertical();
                GUILayout.Space(5);
                EditorUtility.SetDirty( curveData );

            }
            else
            {
                GUILayout.BeginHorizontal();
                m_genTerrain.m_curveDataArray[i] = (CurveData)EditorGUILayout.ObjectField(m_genTerrain.m_curveDataArray[i], typeof(CurveData), true);
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("-"))
                {
                    toRemove = i;
                }
                GUILayout.EndHorizontal();
            }

        }

        if (toRemove != -1)
        {
            m_genTerrain.m_curveDataArray.RemoveAt(toRemove);
        }

        if(GUILayout.Button("Add Curve"))
        {
            m_genTerrain.m_curveDataArray.Add(null);
        }


    }

    private void SetKeys(CurveData a_curveData)
    {
        for (int i = 0; i < m_keyFrames.Length; i++)
        {
            m_keyFrames[i].time = Mathf.InverseLerp(0f, m_keyFrames.Length, i);  

            float newValue = 0;
            float tempKeyMult = (i + m_scrubbing) * m_areaSample;
            switch (a_curveData.m_waveType)
            {
                case WaveType.PERLIN:
                    newValue = Mathf.PerlinNoise(tempKeyMult * a_curveData.m_frequency, 1000);
                    break;
                case WaveType.SIN:
                    newValue = (Mathf.Sin(tempKeyMult * a_curveData.m_frequency) + 1) /2;
                    break;
                case WaveType.SQUARE:
                    newValue = Mathf.Round((Mathf.Sin(tempKeyMult * a_curveData.m_frequency) + 1) /2);
                    break;

            }
            //Debug.Log(newValue);
            newValue = Mathf.Clamp(newValue, a_curveData.m_low, a_curveData.m_high);

            float newAffectValue = 0;
            for (int affect = 0; affect < a_curveData.m_affect.Count; affect++) 
            {
                if (a_curveData.m_affect[affect] != null && a_curveData.m_affect[affect].m_isActive)
                {
                    switch (a_curveData.m_affect[affect].m_waveType)
                    {
                        case WaveType.PERLIN:
                            newAffectValue = Mathf.PerlinNoise(tempKeyMult * a_curveData.m_affect[affect].m_frequency, 1000);
                            break;
                        case WaveType.SIN:
                            newAffectValue = (Mathf.Sin(tempKeyMult * a_curveData.m_affect[affect].m_frequency) + 1) /2;
                            break;
                        case WaveType.SQUARE:
                            newAffectValue = Mathf.Round((Mathf.Sin(tempKeyMult * a_curveData.m_affect[affect].m_frequency) + 1) /2);
                            break;
                    }
                    newAffectValue = Mathf.Clamp(newAffectValue, a_curveData.m_affect[affect].m_low, a_curveData.m_affect[affect].m_high);

                    switch (a_curveData.m_affect[affect].m_blendMode)
                    {
                        case BlendMode.ADD:
                            newValue += (newAffectValue * a_curveData.m_affect[affect].m_amplitude);
                            break;
                        case BlendMode.SUBTRACT:
                            
                            newValue -= (newAffectValue * a_curveData.m_affect[affect].m_amplitude);

                            break;
                        case BlendMode.MULTIPLY:
                            newValue *= (newAffectValue * a_curveData.m_affect[affect].m_amplitude);
                            break;
                        case BlendMode.DIVIDE:
                            newValue /= (newAffectValue * a_curveData.m_affect[affect].m_amplitude);
                            break;

                    }
                    //newValue = (newAffectValue * a_curveData.m_affect[affect].m_amplitude);

                }

            }


            m_keyFrames[i].value = newValue;

            //m_keyFrames[i].tangentMode = 4;


        }

        m_keyFrames[0].value = 0;
        m_keyFrames[m_keyFrames.Length - 1].value = 1;

        m_noiseDisplay = new AnimationCurve(m_keyFrames);
    }

    private void SetKeysAffect(CurveAffectData a_curveAffectData)
    {
        for (int i = 0; i < m_keyFrames.Length; i++)
        {
            m_keyFrames[i].time = Mathf.InverseLerp(0f, m_keyFrames.Length, i);  

            float newValue = 0;
            float tempKeyMult = (i + m_scrubbing) * m_areaSample;
            switch (a_curveAffectData.m_waveType)
            {
                case WaveType.PERLIN:
                    newValue = Mathf.PerlinNoise(tempKeyMult * a_curveAffectData.m_frequency, 1000);
                    break;
                case WaveType.SIN:
                    newValue = (Mathf.Sin(tempKeyMult * a_curveAffectData.m_frequency) + 1) /2;
                    break;
                case WaveType.SQUARE:
                    newValue = Mathf.Round((Mathf.Sin(tempKeyMult * a_curveAffectData.m_frequency) + 1) /2);
                    break;

            }
            //Debug.Log(newValue);
            newValue = Mathf.Clamp(newValue, a_curveAffectData.m_low, a_curveAffectData.m_high);

            m_keyFrames[i].value = newValue;
        }

        m_keyFrames[0].value = 0;
        m_keyFrames[m_keyFrames.Length - 1].value = 1;

        m_noiseDisplay = new AnimationCurve(m_keyFrames);
    }

    private void SetKeysFinal(List<CurveData> a_curveData)
    {
        m_keyFrames = new Keyframe[200];

        for (int curves = 0; curves < a_curveData.Count; curves++)
        {
            if (a_curveData[curves] != null && a_curveData[curves].m_isActive)
            {
                for (int keys = 0; keys < m_keyFrames.Length; keys++)
                {
                    m_keyFrames[keys].time = Mathf.InverseLerp(0f, m_keyFrames.Length, keys);  

                    float newValue = 0;
                    float tempKeyMult = (keys + m_scrubbing) * m_areaSample;
                    switch (a_curveData[curves].m_waveType)
                    {
                        case WaveType.PERLIN:
                            newValue = Mathf.PerlinNoise(tempKeyMult * a_curveData[curves].m_frequency, 1000);
                            break;
                        case WaveType.SIN:
                            newValue = (Mathf.Sin(tempKeyMult * a_curveData[curves].m_frequency) + 1) /2;
                            break;
                        case WaveType.SQUARE:
                            newValue = Mathf.Round((Mathf.Sin(tempKeyMult * a_curveData[curves].m_frequency) + 1) /2);
                            break;

                    }
                    //Debug.Log(newValue);
                    newValue = Mathf.Clamp(newValue, a_curveData[curves].m_low, a_curveData[curves].m_high);

                    float newAffectValue = 0;
                    for (int affect = 0; affect < a_curveData[curves].m_affect.Count; affect++) 
                    {
                        if (a_curveData[curves].m_affect[affect] != null && a_curveData[curves].m_affect[affect].m_isActive)
                        {
                            switch (a_curveData[curves].m_affect[affect].m_waveType)
                            {
                                case WaveType.PERLIN:
                                    newAffectValue = Mathf.PerlinNoise(tempKeyMult * a_curveData[curves].m_affect[affect].m_frequency, 1000);
                                    break;
                                case WaveType.SIN:
                                    newAffectValue = (Mathf.Sin(tempKeyMult * a_curveData[curves].m_affect[affect].m_frequency) + 1) /2;
                                    break;
                                case WaveType.SQUARE:
                                    newAffectValue = Mathf.Round((Mathf.Sin(tempKeyMult * a_curveData[curves].m_affect[affect].m_frequency) + 1) /2);
                                    break;
                            }
                            newAffectValue = Mathf.Clamp(newAffectValue, a_curveData[curves].m_affect[affect].m_low, a_curveData[curves].m_affect[affect].m_high);

                            switch (a_curveData[curves].m_affect[affect].m_blendMode)
                            {
                                case BlendMode.ADD:
                                    newValue += (newAffectValue * a_curveData[curves].m_affect[affect].m_amplitude);
                                    break;
                                case BlendMode.SUBTRACT:

                                    newValue -= (newAffectValue * a_curveData[curves].m_affect[affect].m_amplitude);

                                    break;
                                case BlendMode.MULTIPLY:
                                    newValue *= (newAffectValue * a_curveData[curves].m_affect[affect].m_amplitude);
                                    break;
                                case BlendMode.DIVIDE:
                                    newValue /= (newAffectValue * a_curveData[curves].m_affect[affect].m_amplitude);
                                    break;

                            }
                            //newValue = (newAffectValue * a_curveData.m_affect[affect].m_amplitude);

                        }

                    }

                    switch (a_curveData[curves].m_blendMode)
                    {
                        case BlendMode.ADD:
                            m_keyFrames[keys].value += (newValue * a_curveData[curves].m_amplitude);
                            break;
                        case BlendMode.SUBTRACT:
                            m_keyFrames[keys].value -= (newValue * a_curveData[curves].m_amplitude);
                            break;
                        case BlendMode.MULTIPLY:
                            m_keyFrames[keys].value *= (newValue * a_curveData[curves].m_amplitude);
                            break;
                        case BlendMode.DIVIDE:
                            m_keyFrames[keys].value /= (newValue * a_curveData[curves].m_amplitude);
                            break;

                    }




                    //m_keyFrames[i].tangentMode = 4;


                } 
            }

        }




        //m_keyFrames[0].value = 0;
        //m_keyFrames[m_keyFrames.Length - 1].value = 1;

        m_noiseDisplay = new AnimationCurve(m_keyFrames);
    }


}
