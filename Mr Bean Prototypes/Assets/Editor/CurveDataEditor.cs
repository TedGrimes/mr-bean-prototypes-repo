﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CurveData))]
public class CurveDataEditor : Editor {

    CurveData m_curveData;

    private float m_propSpacing = 5;
    private float m_labelSpacing = 10;


    [SerializeField]
    private AnimationCurve m_noiseDisplay;
    private Keyframe[] m_keyFrames;


    public void OnEnable()
    {
        m_curveData = (CurveData)target;
        if (m_curveData.m_name == "")
        {
            m_curveData.m_name = m_curveData.name;
        }
        m_keyFrames = new Keyframe[200];
        m_noiseDisplay = new AnimationCurve(m_keyFrames);
        SetKeys();
    }

    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck ();

        GUI.color = Color.grey;
        GUILayout.BeginVertical(EditorStyles.helpBox);
        GUI.color = Color.white;

        EditorGUILayout.LabelField(m_curveData.name, EditorStyles.boldLabel);

        GUILayout.Space(m_labelSpacing);

        m_curveData.m_waveType = (WaveType)EditorGUILayout.EnumPopup("Wave Type", m_curveData.m_waveType);
        GUILayout.Space(m_propSpacing);
        //m_curveData.m_blendMode = (BlendMode)EditorGUILayout.EnumPopup("Blend Mode", m_curveData.m_blendMode);
        //GUILayout.Space(m_propSpacing);
        m_curveData.m_frequency = EditorGUILayout.FloatField("Frequency", m_curveData.m_frequency);
        GUILayout.Space(m_propSpacing);
        //m_curveData.m_amplitude = EditorGUILayout.FloatField("Amplitude", m_curveData.m_amplitude);
        //GUILayout.Space(m_labelSpacing);

        EditorGUILayout.LabelField("Limits", EditorStyles.boldLabel);
        GUILayout.Space(m_propSpacing);
        m_curveData.m_low = EditorGUILayout.Slider("Low", m_curveData.m_low, 0f, 1f);
        GUILayout.Space(m_propSpacing);
        m_curveData.m_high = EditorGUILayout.Slider("Heigh", m_curveData.m_high, 0f, 1f);




        GUILayout.EndVertical();



        //EditorGUIUtility.DrawCurveSwatch(new Rect(0, 0, 100, 100), m_noiseDisplay);
        GUILayout.Space(m_labelSpacing);
        EditorGUILayout.LabelField("Noise Example", EditorStyles.boldLabel);

        EditorGUILayout.CurveField(m_noiseDisplay,GUILayout.Height(150));
       


        if (EditorGUI.EndChangeCheck ()) {

            SetKeys();
            Repaint();
        }



        EditorUtility.SetDirty( m_curveData );
    }

    private void SetKeys()
    {
        for (int i = 0; i < m_keyFrames.Length; i++)
        {
            m_keyFrames[i].time = Mathf.InverseLerp(0f, m_keyFrames.Length, i);  

            float newValue = 0;
            switch (m_curveData.m_waveType)
            {
                case WaveType.PERLIN:
                    newValue = Mathf.PerlinNoise(i * m_curveData.m_frequency, 1000);
                    break;
                case WaveType.SIN:
                    newValue = (Mathf.Sin(i * m_curveData.m_frequency) + 1) /2;
                    break;
                case WaveType.SQUARE:
                    newValue = Mathf.Round((Mathf.Sin(i * m_curveData.m_frequency) + 1) /2);
                    break;

            }
            //Debug.Log(newValue);
            newValue = Mathf.Clamp(newValue, m_curveData.m_low, m_curveData.m_high);

            m_keyFrames[i].value = newValue;

            //m_keyFrames[i].tangentMode = 4;


        }

        m_keyFrames[0].value = 0;
        m_keyFrames[m_keyFrames.Length - 1].value = 1;

        m_noiseDisplay = new AnimationCurve(m_keyFrames);
    }
	
}
