﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class CreateCurveDataScriptable {

    [MenuItem("Assets/Create/CurveData")]
    public static void CreateCurveData () {
        CurveData curveData = ScriptableObject.CreateInstance<CurveData>();
        AssetDatabase.CreateAsset(curveData, "Assets/Resources/Curves/CurveData" + (AssetDatabase.LoadAllAssetsAtPath("Assets/Resources/Curves").Length + 1) + ".asset");
        AssetDatabase.SaveAssets();
	}
	
}

[System.Serializable]
public class CreateCurveAffectDataScriptable {

    [MenuItem("Assets/Create/CurveAffectData")]
    public static void CreateCurveData () {
        CurveAffectData curveData = ScriptableObject.CreateInstance<CurveAffectData>();
        AssetDatabase.CreateAsset(curveData, "Assets/Resources/CurveAffect/CurveAffectData" + (AssetDatabase.LoadAllAssetsAtPath("Assets/Resources/CurveAffect").Length + 1) + ".asset");
        AssetDatabase.SaveAssets();
    }

}
