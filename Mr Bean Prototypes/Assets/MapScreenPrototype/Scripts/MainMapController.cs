﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MainMapController : MonoBehaviour {

	public House[] m_Houses;
	public House m_SelectHouse;

	[System.Serializable]
	public class UIElement
	{
		public RectTransform m_ElementRect;
		public Vector2 m_OnScreenPosition;
		public Vector2 m_OffScreenPosition;
		public AnimationCurve m_TransitionCurve;
		public float m_TransitionSpeed = 1;
	}

	public UIElement m_HousePanel;

	[System.Serializable]
	public class CharacterElementUI
	{
		public Image m_Image;
		public RectTransform m_PackageIconStartPos;
		public RectTransform m_PackageIconEndPos;
		public List<RectTransform> m_LoadedIcons = new List<RectTransform>();
        public Button m_SkipButton;
	}

	public CharacterElementUI[] m_characterEmelentUI;

	public Image[] m_PackageIconImages;
	public Sprite[] m_CharacterImages;

	void Start () {


		m_HousePanel.m_ElementRect.anchoredPosition = m_HousePanel.m_OffScreenPosition;


	}
	

	void Update () {
        
	}

	IEnumerator HousePanelOnScreen(int houseRef)
	{
		for (int i = 0; i < 3; i++)
		{
			for (int p = 0; p < m_characterEmelentUI[i].m_LoadedIcons.Count; p++)
			{
				Destroy(m_characterEmelentUI[i].m_LoadedIcons[p].gameObject);
			}
			m_characterEmelentUI[i].m_LoadedIcons = new List<RectTransform>();
			m_characterEmelentUI[i].m_Image.sprite = m_CharacterImages[Random.Range(0, 3)];
            m_characterEmelentUI[i].m_SkipButton.gameObject.SetActive(true);
		}


		//m_HousePanel.m_ElementRect.anchoredPosition = m_HousePanel.m_OnScreenPosition;
		m_SelectHouse = m_Houses[houseRef];
		for (int i = 0; i < 3; i++)
		{
			//m_characterEmelentUI[i].m_Image.sprite = m_SelectHouse.m_Characters[i].m_Image;

			int randPackage = Random.Range(1, 10);
			m_SelectHouse.m_Characters[i].m_Packages = new List<int>();
			for (int p = 0; p < randPackage; p++) {
				m_SelectHouse.m_Characters[i].m_Packages.Add((int) Random.Range(0, 3));

            }


			for (int p = 0; p < m_SelectHouse.m_Characters[i].m_Packages.Count; p++) {
				RectTransform newIcon = Instantiate(m_PackageIconImages[m_SelectHouse.m_Characters[i].m_Packages[p]]).GetComponent<RectTransform>();
				newIcon.parent =  m_characterEmelentUI[i].m_PackageIconEndPos.parent;
				newIcon.anchoredPosition = Vector2.Lerp(m_characterEmelentUI[i].m_PackageIconStartPos.anchoredPosition,m_characterEmelentUI[i].m_PackageIconEndPos.anchoredPosition, Mathf.InverseLerp(0, 10, p));

				m_characterEmelentUI[i].m_LoadedIcons.Add(newIcon);


            }

		}



		float t = 0;

		while (t < 1) 
		{
			m_HousePanel.m_ElementRect.anchoredPosition = Vector2.LerpUnclamped(m_HousePanel.m_OffScreenPosition, m_HousePanel.m_OnScreenPosition, m_HousePanel.m_TransitionCurve.Evaluate(t));

			t += Time.deltaTime * m_HousePanel.m_TransitionSpeed;
			yield return null;
		}

		yield return null;
	}

	IEnumerator HousePanelOffScreen()
	{
		//m_HousePanel.m_ElementRect.anchoredPosition = m_HousePanel.m_OffScreenPosition;

		float t = 0;

		while (t < 1) 
		{
			m_HousePanel.m_ElementRect.anchoredPosition = Vector2.LerpUnclamped(m_HousePanel.m_OnScreenPosition,m_HousePanel.m_OffScreenPosition, m_HousePanel.m_TransitionCurve.Evaluate(t));

			t += Time.deltaTime * m_HousePanel.m_TransitionSpeed;
			yield return null;
		}

		yield return null;
	}

	public void LoadLevel(int i)
	{
		PlayerPrefs.SetInt ("ObjectListLength", m_SelectHouse.m_Characters[i].m_Packages.Count);

		for (int p = 0; p < m_SelectHouse.m_Characters[i].m_Packages.Count; p++) {
			PlayerPrefs.SetInt ("Object" + p, m_SelectHouse.m_Characters[i].m_Packages[p]);
		}
		PlayerPrefs.Save();
        SceneManager.LoadScene("CarSelectScene");
	}

	public void SkipCharacter(int i)
	{
		for (int p = 0; p < m_characterEmelentUI[i].m_LoadedIcons.Count; p++)
		{
			Destroy(m_characterEmelentUI[i].m_LoadedIcons[p].gameObject);
		}
		m_characterEmelentUI[i].m_LoadedIcons = new List<RectTransform>();

		int randPackage = Random.Range(1, 10);
		m_SelectHouse.m_Characters[i].m_Packages = new List<int>();
		for (int p = 0; p < randPackage; p++) {
			m_SelectHouse.m_Characters[i].m_Packages.Add((int) Random.Range(0, 3));

		}


		for (int p = 0; p < m_SelectHouse.m_Characters[i].m_Packages.Count; p++) {
			RectTransform newIcon = Instantiate(m_PackageIconImages[m_SelectHouse.m_Characters[i].m_Packages[p]]).GetComponent<RectTransform>();
			newIcon.parent =  m_characterEmelentUI[i].m_PackageIconEndPos.parent;
			newIcon.anchoredPosition = Vector2.Lerp(m_characterEmelentUI[i].m_PackageIconStartPos.anchoredPosition,m_characterEmelentUI[i].m_PackageIconEndPos.anchoredPosition, Mathf.InverseLerp(0, 10, p));

			m_characterEmelentUI[i].m_LoadedIcons.Add(newIcon);


		}
		m_characterEmelentUI[i].m_Image.sprite = m_CharacterImages[Random.Range(0, 3)];
        m_characterEmelentUI[i].m_SkipButton.gameObject.SetActive( false);
	}

	public void SelectHouse(int i)
	{
		StartCoroutine (HousePanelOnScreen (i));
	}

	public void DeselectHouse()
	{
		StartCoroutine (HousePanelOffScreen ());
	}
}
