﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class House 
{

	public List<Character> m_Characters = new List<Character> ();

}
[System.Serializable]
public class Character
{
	public string m_Name = "Character";
	public Sprite m_Image = null;
	public List<int> m_Packages = new List<int> ();

}


