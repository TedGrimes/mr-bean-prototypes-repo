﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WaveType {PERLIN, SIN, SQUARE}
public enum BlendMode {ADD, SUBTRACT, MULTIPLY, DIVIDE} 

[System.Serializable]
public class CurveData : ScriptableObject {

    public string m_name = "";
    public WaveType m_waveType = WaveType.PERLIN;
    public BlendMode m_blendMode = BlendMode.ADD;
    public float m_frequency = 0.1f;
    public float m_amplitude = 1;
    public float m_low = 0;
    public float m_high = 1;
    public bool m_isActive = true;

    public List<CurveAffectData> m_affect = new List<CurveAffectData>();
}
